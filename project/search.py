import os
import math
from pymystem3 import Mystem
import re
import numpy

def idf_func (words, docs):  #создаем список значений idf
	idf_arr = []
	for word in words:
		df = 0
		for doc in docs:
			if doc.get(word):
				df += 1
		idf = math.log10 (len(docs) / df )
		idf_arr.append(idf)
	return idf_arr

m = Mystem()

def Vec (array, tf, idf_arr): #вектор весов для каждого документа
	vec = [0] * len(allWords)
	for i in range (len(allWords)):
		if allWords[i] in tf:
			vec[i] = tf[ allWords[i] ] * idf_arr[i] 
	return vec

def znam (vec):
	res = 0
	for i in range (len(vec)):
		res += vec[i] * vec[i]
	res = math.sqrt(res)
	return res				

def normalize (Vec):
	z = znam(Vec)
	if z == 0:
		return Vec
	for i in range( len(Vec)):
		Vec[i] = Vec[i] / z
	return Vec

def cos (vec1, vec2):
	if math.sqrt (numpy.dot(vec1, vec1)) == 0 or math.sqrt(numpy.dot(vec2, vec2)) == 0:
		return 0.0
	return numpy.dot(vec1, vec2) / (math.sqrt(numpy.dot(vec1, vec1)) * math.sqrt(numpy.dot(vec2, vec2)))
	
files = os.listdir()
files_txt = list(filter(lambda x: x.endswith('.txt') , files))  #список всех доков
texts = []
for f in files_txt:
	file = open(f, 'r')
	str = file.read()
	texts.extend(str.split('.'))

texts = list(filter(lambda x: x!= '', texts))

dicts = []
allWords = [] #список всех термов
tf_sen = [] #все леммы-словарь
for text in texts:
	if text!='':
		dictionary = dict()
		dicts = m.lemmatize(text)
		for d in dicts:
			if d.isalpha():
				if d not in allWords:
					allWords.append(d)
				if d not in dictionary:
					dictionary[d] = 1
				else:
					dictionary[d] += 1
		tf_sen.append(dictionary)

question = input()
q = []
d1 = dict()#словарь запроса
q = m.lemmatize(question)
for d in q:
	if d.isalpha():
		if d not in d1:
			d1[d] = 1
		else:
			d1[d] += 1

idf_arr = idf_func(allWords, tf_sen) #считаем idf 

quesVec = Vec(allWords, d1, idf_arr)

quesVecNorm = normalize(quesVec) #нормализованный вектор запроса

docVec = [] #векторы документов
for tf in tf_sen:
	docVec.append( Vec(allWords, tf, idf_arr))

for i in range( len( docVec)):
	docVec[i] = normalize(docVec[i])#нормализованные векторы документов

search_list = dict()
for i in range ( len (docVec)):
	num_cos = cos(quesVecNorm, docVec[i])#считаем веса по отношению к запросу 
	search_list[i] = num_cos

list_items = list(search_list.items())
list_items.sort(key = lambda i:i[1])
list_items.reverse()

for i in range(100):
	num = list_items[i][0]
	print('Документ: '+texts[num]+ ' с весом: ', (list_items[i][1]))
	print()
print("########################")
##################
for i in range (len(tf_sen)):
	for key in tf_sen[i].keys():
		if tf_sen[i][key] != 0:
			tf_sen[i][key] = 0.4 + 0.6 * tf_sen[i][key] / numpy.max(list(tf_sen[i].values()))

for key in d1.keys():
	if d1[key] != 0:
		d1[key] = 0.4 + 0.6 * d1[key] / numpy.max(list(d1.values()))

quesVec_1 = Vec(allWords, d1, idf_arr)
quesVecNorm_1 = normalize(quesVec_1) #нормализованный вектор запроса
docVec_1 = [] #векторы документов
for tf in tf_sen:
	docVec_1.append( Vec(allWords, tf, idf_arr))

for i in range( len( docVec_1)):
	docVec_1[i] = normalize(docVec_1[i])#нормализованные векторы документов

search_list_1 = dict()
for i in range ( len (docVec_1)):
	num_cos = cos(quesVecNorm_1, docVec_1[i])#считаем веса по отношению к запросу 
	search_list_1[i] = num_cos

list_items_1 = list(search_list_1.items())
list_items_1.sort(key = lambda i:i[1])
list_items_1.reverse()

for i in range(10):
	num = list_items_1[i][0]
	print('Документ: '+texts[num]+ ' с весом: ', (list_items_1[i][1]))
	print()


